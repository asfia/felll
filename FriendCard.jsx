// FriendCard.js
import React, { useState } from 'react';
import Modal from './Modal';
import './FriendCard.css';
import { v4 as uuidv4 } from 'uuid'; // Import uuidv4 function

const FriendCard = ({ friend }) => {
  const [showModal, setShowModal] = useState(false);
  const cardId = uuidv4(); // Generate unique ID for the FriendCard

  const handleClick = () => {
    setShowModal(true);
    // Set z-index of clicked card to ensure it's on top
    document.getElementById(cardId).style.zIndex = 9999;
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <div className="friend-card" id={cardId}>
      <img src={friend.picture.large} alt="Friend" onClick={handleClick} />
      <h3>{friend.name.first} {friend.name.last}</h3>
      <p>Email: {friend.email}</p>
      <p>Phone: {friend.phone}</p>
      {showModal && <Modal friend={friend} onClose={handleCloseModal} cardId={cardId} />} {/* Pass the cardId */}
    </div>
  );
};

export default FriendCard;
