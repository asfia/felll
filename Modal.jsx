// Modal.js
import React from 'react';
import './FriendCard.css';

const Modal = ({ friend, onClose, cardId, modalId }) => {
  // Determine if this modal should be active based on cardId and modalId
  const isActive = cardId === modalId;

  return (
    <div className={`modal ${isActive ? 'active' : ''}`} onClick={isActive ? null : onClose}>
      <div className="modal-content" onClick={(e) => e.stopPropagation()}>
        <span className="close" onClick={onClose}>&times;</span>
        <img src={friend.picture.large} alt="Friend" />
        <h3>{friend.name.first} {friend.name.last}</h3>
        <p>Email: {friend.email}</p>
        <p>Date of Birth: {new Date(friend.dob.date).toLocaleDateString()}</p>
        <p>Address: {friend.location.street.number} {friend.location.street.name}, {friend.location.city}, {friend.location.state}, {friend.location.country}</p>
        <p>Phone: {friend.phone}</p>
      </div>
    </div>
  );
};

export default Modal;
